<?php 
session_start();

require_once("vendor/autoload.php");

use \Slim\Slim;

$app = new \Slim\Slim();

$app->config('debug', true);

$app->get('/', function() {

	$page = new PageAdmin([
		"header"=>false,
		"footer"=>false
	]);

	$page->setTpl("login");

});

$app->post('/login', function() {

	User::login($_POST['login'], $_POST['password']);

	header("Location: /home-login");
	exit;

});

$app->run();

 ?>