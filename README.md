1.0 Entrar na pasta "C:\Windows\System32\drivers\etc"
1.1 Alterar o arquivo "host", colocando o código na ultima linha

Código:

127.0.0.1		www.nomesite.com.br

2.0 Entrar na pasta "C:\xampp\apache\conf\extra"
2.1 Alterar o arquivo "httpd-vhosts.conf", colocando o código na ultima linha

Código:

<VirtualHost *:80>
    ServerAdmin filipeodev@gmail.com
    DocumentRoot "C:/site-base-slim"
    ServerName www.site-base-slim.com.br
    ErrorLog "logs/dummy-host2.example.com-error.log"
    CustomLog "logs/dummy-host2.example.com-access.log" common
    <Directory "C:/site-base-slim">
        Require all granted

        RewriteEngine On

        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [QSA,L]
    </Directory>
</VirtualHost>

3.0 Ir até a raiz da pasta do projeto, apertar com botão direito, acessar o git bash

Cógido: 

"composer update"

3.2 Criar uma pasta chamada "fili" dentro de vendo
3.3 Criar uma pasta chamada "php-classes" dentro de "fili".
3.4 Colocar o arquivo "src" dentro da pasta "php-classes".
3.5 Ir até a raiz da pasta do projeto, apertar com botão direito, acessar o git bash

Código:

composer dump-autoload